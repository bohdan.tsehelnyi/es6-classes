// ================================THEORY====================================
/*
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.

У кожному обєкті є внутрішній обєкт прототип і його основна роль це брати участь у наслідуванні відповідних даних у обєктів між собою. Сама властивість [prototype] грає ключову роль у наслідуванні і передаванні даних з одного обєкта в іниший.

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

Ключове слово super() нам потрібно для виклику або перенесення методів батьківського класу в конструктор нового класу нащадка. Можна сказати відтворення методів батьківського класу для подальшої взаємодії з методами нащадка.
*/

// ==============================PRACTICE=====================================

class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

        get info(){
            return `This employee is ${this._name} and he is ${this._age} and he has salary ${this._salary} $`;
        }
        set name(newName){
            newName = newName.trim();
           if(newName === ''){
            throw "Name can\'t be empty!!!"
           }
           this._name = newName;
        }
        set age(newAge){
            if(newAge > 30){
           throw "He is old man!!!"
           }
            this._age = newAge;
        }
        set salary(newSalary){
            if(newSalary > 2000){
           throw "He is rich!!!"
           }
            this._salary = newSalary;
        }
}
let employee = new Employee("Ann", 20, 2000);
console.log(employee.info);
employee.name = "Dan";
console.log(employee.info);

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang;
    }
    set multiply(mult){
        this._salary = mult;
        this._salary *= 3;
    }
    get multiply(){
        return `This employee is ${this._name} and he is ${this._age} and he has salary ${this._salary} $ and he is speaking ${this._lang}`;
    }
  
}
let employee2 = new Programmer("Rich", 29, 2000, "English");
employee2.multiply = 2200;
console.log(employee2.multiply);

let employee3 = new Programmer("Sofia", 18, 1000, "Spain");
employee3.multiply = 1200;
console.log(employee3.multiply);

let employee4 = new Programmer("Bob", 23, 1000, "Spain");
employee4.multiply = 1500;
console.log(employee4.multiply);